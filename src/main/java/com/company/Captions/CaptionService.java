package com.company.Captions;

public interface CaptionService {
    String getCaption(final String key);
}
