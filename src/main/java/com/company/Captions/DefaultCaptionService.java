package com.company.Captions;

import java.util.HashMap;
import java.util.Map;

public class DefaultCaptionService implements CaptionService {
    final private static Map<String, String> captionMap = getDefaultCaptions();

    @Override
    public String getCaption(String key) {
        return captionMap.get(key);
    }

    private static Map<String,String> getDefaultCaptions() {
        Map<String,String> captions = new HashMap<>();
        captions.put("menu.mainMenu","*****************************\n" +
                "*   1. Add new ToDo task;   *" +
                "\n*   2. Print out tasks;     *" +
                "\n*   3. Update task;         *" +
                "\n*   4. Delete task;         *" +
                "\n*   5. Settings;            *" +
                "\n*   6. Quit;                *" +
                "\n*****************************");
        captions.put("menu.addTaskMenu", "1. Reminder\n" +
                "2. Meeting\n" +
                "3. Reserve time");
        captions.put("message.printOutTasks","Print out tasks:");
        captions.put("menu.printOut","Select what to print:" +
                "\n1. List tasks sorted by name." +
                "\n2. List tasks sorted by start time." +
                "\n3. List only by category");
        captions.put("menu.printOutCategory", "1. Reminders\n2. Meetings\n3. Reserve times");
        captions.put("message.deleteTask","Delete task:");
        captions.put("message.updateTask", "Select which task to update:");
        captions.put("errorMessage.noTaskToDelete", "There is no task like that");
        captions.put("taskType.reminder","reminder");
        captions.put("taskType.meeting","meeting");
        captions.put("taskType.reserveTime","reserve time");
        captions.put("file.name","ToDoList.txt");
        captions.put("taskElement.name", "Enter new name:");
        captions.put("taskElement.startTime", "Start time:");
        captions.put("taskElement.duration", "Duration:");
        captions.put("taskElement.location", "Location:");
        captions.put("taskElement.numberOfAttendees", "Number of attendees:");
        captions.put("taskElement.listOfAttendees", " attendees name");
        captions.put("errorMessage.NothingToDelete", "There is no element with this id");
        captions.put("errorMessage.NoTask", "There is no task, first create a new task.");
        captions.put("message.firstLogin","Welcome this is your first time.\nPlease select what task you want to create:");
        captions.put("taskElement.numberOfAttendeesChange", "Do you wish to change list of attendees? (Yes/No)");
        captions.put("message.taskEnded", " task has ended, you can select to remove task that has ended");
        captions.put("message.reminderInterval", "Enter new value for reminder notification interval (default minutes, yyyy:MM:dd:HH:mm)");
        captions.put("message.oldNameLeft", " will be left as name.");
        return captions;
    }
}
