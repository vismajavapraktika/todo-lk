package com.company.Captions;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class PrintCaption {
    @Autowired
    private CaptionService captionService;

    public void getPrintCaption(String captionName){
        System.out.println(captionService.getCaption(captionName));
    }
}
