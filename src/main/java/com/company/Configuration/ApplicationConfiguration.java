package com.company.Configuration;

import com.company.Captions.CaptionService;
import com.company.Captions.DefaultCaptionService;
import com.company.Captions.PrintCaption;
import com.company.Configuration.Properties.FileProperties;
import com.company.Data.*;
import com.company.Files.PrintToFile;
import com.company.Files.PrintToTextFile;
import com.company.Files.ReadFromFiles;
import com.company.Files.ReadFromTextFile;
import com.company.JDBC.DatabasePullInformation;
import com.company.JDBC.DatabasePushInformation;
import com.company.Tasks.*;
import com.google.common.base.Objects;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ApplicationConfiguration {

    @Autowired
    private FileProperties fileProperties;

    @Bean
    public PrintToFile printToFile() {
        final FileType type = FileType.valueOf(fileProperties.getType());

        if (Objects.equal(type, FileType.TEXT_FILE)) {
            return new PrintToTextFile();
        }
        if (Objects.equal(type, FileType.DATA_BASE)) {
            return new DatabasePushInformation();
        }

        return new PrintToTextFile();
    }

    @Bean
    public ReadFromFiles readFromFiles() {
        final FileType type = FileType.valueOf(fileProperties.getType());

        if (Objects.equal(type, FileType.TEXT_FILE)) {
            return new ReadFromTextFile();
        }
        if (Objects.equal(type, FileType.DATA_BASE)) {
            return new DatabasePullInformation();
        }
        return new DatabasePullInformation();
    }

    private enum FileType {
        TEXT_FILE,
        DATA_BASE
    }

    @Bean
    public InputScanner inputScanner() {
        return new ConsoleInputScanner();
    }

    @Bean
    public PrintTasks printTasks() {
        return new PrintTasksToConsole();
    }

    @Bean
    public PrintSelections printSelections() {
        return new PrintSelections();
    }

    @Bean
    public TaskReminder taskReminder() {
        return new TaskReminder();
    }

    @Bean
    public DataChecker dataChecker() {
        return new DataChecker();
    }

    @Bean
    public CaptionService captionService() {
        return new DefaultCaptionService();
    }

    @Bean
    public TaskListOperations taskListOperations() {
        return new TaskListOperationsWeb();
//        return new TaskListOperationsConsole();
    }

    @Bean
    public PrintCaption printCaption() {
        return new PrintCaption();
    }

    @Bean
    public Task task() {
        return new Task();
    }

}
