package com.company.Data;

import org.springframework.stereotype.Service;

import java.util.Scanner;

@Service
public class ConsoleInputScanner implements InputScanner{
    @Override
    public String getStringInput() {
        final Scanner scanner = new Scanner(System.in);
        return scanner.nextLine();
    }

    @Override
    public int getIntegerInput() {
        final Scanner scanner = new Scanner(System.in);
        int integerInput = scanner.nextInt();
        scanner.nextLine();
        return integerInput;
    }
}
