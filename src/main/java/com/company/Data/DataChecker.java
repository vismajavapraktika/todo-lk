package com.company.Data;

import com.company.Tasks.Task;
import com.company.ToDo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;

import static com.company.Tasks.Operations.UpdateTaskOperation.TaskThatIsUpdatingId;

public class DataChecker {

    private DateFormat format = new SimpleDateFormat("yyyy:MM:dd:HH:mm");

    public boolean inputChecker(String dataType, String input, String taskType) {
        switch (dataType) {
            case "Date":
                if (input.isEmpty() && (taskType.equals("meeting") || taskType.equals("reserve time"))) {
                    return false;
                } else if (taskType.equals("reminder") && input.isEmpty()) {
                    return true;
                } else {
                    Calendar checkingDate = Calendar.getInstance();
                    try {
                        checkingDate.setTime(format.parse(input));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    if (checkingDate.after(Calendar.getInstance())){
                        return true;
                    } else {
                        System.out.println("Enter valid date: yyyy:MM:dd:HH:mm");
                        return false;
                    }
                }
            case "DurationTime":
                if (input.isEmpty() && (taskType.equals("meeting") || taskType.equals("reserve time"))) {
                    return false;
                } else if (taskType.equals("reminder") && input.isEmpty()) {
                    return true;
                } else {
                    //String[] tmp = input.split(":");
//                    return Integer.parseInt(tmp[0]) >= 0 && (Integer.parseInt(tmp[1]) >= 0 && Integer.parseInt(tmp[1]) < 60);
                    return true;
                }
            case "Name":
                return !input.isEmpty();
        }
        return false;
    }


    public boolean overlappingTimeCheck(LocalDateTime startTime, Duration duration, String type) throws ParseException {
        if (startTime != null || duration != null) {
            LocalDateTime endTime = startTime.plus(duration);
            if (ToDo.allTasks.isEmpty()) {
                return true;
            }
            switch (type) {
                case "meeting":
                    return overlappingTime(startTime, endTime, type);
                case "reminder":
                    return overlappingTime(startTime, endTime, type);
                case "reserve time":
                    return overlappingTime(startTime, endTime, type);
            }
            return false;
        } return true;
    }

    private boolean ifStatementInOverlappingTime(LocalDateTime a, LocalDateTime b, LocalDateTime c, LocalDateTime d){
        if (a.isBefore(c)){
            if (c.isBefore(b) || b.equals(d) || d.isBefore(b))
                return false;
        }
        else if (a.equals(c)){
            if (b.isBefore(d) || b.equals(d) || d.isBefore(b)){
                return false;
            }
        }
        else if (c.isBefore(a) && a.isBefore(d)){
            if (b.isBefore(d) || b.equals(d) || d.isBefore(b)){
                return false;
            }
        }
        return true;
    }

    private boolean overlappingTime(LocalDateTime startTime, LocalDateTime endTime, String type) throws ParseException {
        switch (type) {
            case "meeting":
                for (Task allTask : ToDo.allTasks) {
                    if (allTask.getId() != TaskThatIsUpdatingId){
                        if (allTask.getStartTime() != null && allTask.getDuration() != null) {
                            LocalDateTime c = (allTask.getStartTime()), d = allTask.getEndTime();
                            if (allTask.getType().equals("meeting") || allTask.getType().equals("reserve time")) {
                                if (!ifStatementInOverlappingTime(startTime, endTime, c, d)) {
                                    return false;
                                }
                            }
                        }
                    }
                }
                break;
            case "reserve time":
                for (Task allTask : ToDo.allTasks) {
                    if (allTask.getId() != TaskThatIsUpdatingId){
                        if (allTask.getStartTime() != null && allTask.getDuration() != null) {
                            LocalDateTime c = allTask.getStartTime(), d = allTask.getEndTime();
                            if (!ifStatementInOverlappingTime(startTime, endTime, c, d)) {
                                return false;
                            }
                        }
                    }
                }
                break;
            case "reminder":
                for (Task allTask : ToDo.allTasks) {
                    if (allTask.getId() != TaskThatIsUpdatingId){
                        if (allTask.getStartTime() != null && allTask.getDuration()!= null) {
                            LocalDateTime c = allTask.getStartTime(), d = allTask.getEndTime();
                            if (allTask.getType().equals("reserve time")) {
                                if (!ifStatementInOverlappingTime(startTime, endTime, c, d)) {
                                    return false;
                                }
                            }
                        }
                    }
                }
                break;
        }
        return true;
    }
}
