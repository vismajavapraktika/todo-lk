package com.company.Data;

public interface InputScanner {
    String getStringInput();
    int getIntegerInput();
}
