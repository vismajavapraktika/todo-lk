package com.company.Data;

import com.company.Captions.PrintCaption;
import org.springframework.beans.factory.annotation.Autowired;


@SuppressWarnings("WeakerAccess")
public class PrintSelections {
    @Autowired
    private PrintTasks printTasks;
    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private PrintCaption printCaption;

    void printOutSelections(int selection){
        switch (selection){
            case 1:
                printTasks.printTasksByName();
                break;
            case 2:
                printTasks.printTasksByStartTime();
                break;
            case 3:
                printCaption.getPrintCaption("menu.printOutCategory");
                printTasks.printTaskByCategory(inputScanner.getIntegerInput());
        }

    }
}
