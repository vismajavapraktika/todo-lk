package com.company.Data;

public interface PrintTasks {
    void printOutTasks();
    void printTasksByName();
    void printTasksByStartTime();
    void printTaskByCategory(final int taskType);
}
