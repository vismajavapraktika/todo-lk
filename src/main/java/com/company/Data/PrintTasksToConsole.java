package com.company.Data;

import com.company.Tasks.Task;
import com.company.ToDo;

import java.util.ArrayList;
import java.util.Collections;

import static com.company.Tasks.Task.taskTypeMeeting;
import static com.company.Tasks.Task.taskTypeReminder;
import static com.company.Tasks.Task.taskTypeReserveTime;

public class PrintTasksToConsole implements PrintTasks {

    @Override
    public void printOutTasks() {
        for (Task allTask : ToDo.allTasks){
            System.out.println(allTask.toStringPrintOut());
        }
    }

    @Override
    public void printTasksByName() {
        ArrayList<Task> copyTasks = new ArrayList<>(ToDo.allTasks);
        Collections.copy(copyTasks,ToDo.allTasks);
        Collections.sort(copyTasks, (o1, o2) -> o1.getTaskName().compareTo(o2.getTaskName()));
        for (Task task : copyTasks) {
            System.out.println(task.toStringByName());
        }
    }

    @Override
    public void printTasksByStartTime() {
        ArrayList<Task> copyTasks = new ArrayList<>(ToDo.allTasks);
        Collections.copy(copyTasks,ToDo.allTasks);
        Collections.sort(copyTasks, (o1, o2) -> o1.getStartTime().compareTo(o2.getStartTime()));
        for (Task task : copyTasks) {
            System.out.println(task.toStringByName());
        }
    }

    @Override
    public void printTaskByCategory(final int taskType) {
        // 1 Reminder 2. meeting 3. reserve time
        switch (taskType){
            case 1:
                for (int i = 0; i < ToDo.allTasks.size(); i++){
                    if (ToDo.allTasks.get(i).getType().equals(taskTypeReminder)){
                        System.out.println(ToDo.allTasks.get(i).toStringByName());
                    }
                }
                break;
            case 2:
                for (int i = 0; i < ToDo.allTasks.size(); i++){
                    if (ToDo.allTasks.get(i).getType().equals(taskTypeMeeting)){
                        System.out.println(ToDo.allTasks.get(i).toStringByName());
                    }
                }
                break;
            case 3:
                for (int i = 0; i < ToDo.allTasks.size(); i++){
                    if (ToDo.allTasks.get(i).getType().equals(taskTypeReserveTime)){
                        System.out.println(ToDo.allTasks.get(i).toStringByName());
                    }
                }
                break;
        }
    }
}