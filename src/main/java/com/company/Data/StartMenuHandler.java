package com.company.Data;

import com.company.Captions.PrintCaption;
import com.company.Tasks.Repository.TaskService;
import com.company.Tasks.Task;
import com.company.Tasks.TaskReminder;
import com.company.Tasks.TaskListOperations;
import com.company.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.List;

import static com.company.Tasks.Task.taskTypeMeeting;
import static com.company.Tasks.Task.taskTypeReminder;
import static com.company.Tasks.Task.taskTypeReserveTime;

@Controller
public class StartMenuHandler extends Thread{
   @Autowired
    private PrintCaption printCaption;
    @Autowired
    private TaskListOperations taskListOperations;
//    @Autowired
    private InputScanner inputScanner = new ConsoleInputScanner();
    //@Autowired
    private PrintTasks printTasks = new PrintTasksToConsole();
    @Autowired
    private PrintSelections printSelections;
//    @Autowired
    private TaskReminder taskReminder = new TaskReminder();
    private int selection;

    public void start(int selection){
        this.selection = selection;
        run();
    }

    @Override
    public void run() {
            try {
                handleStartMenuInput(this.selection);
            } catch (ParseException | FileNotFoundException e) {
                e.printStackTrace();
            }
    }

    private void handleStartMenuInput(final int input) throws ParseException, FileNotFoundException {
        switch (input){
            case 1: // Add new task
                addNewTaskSelection();
                break;
            case 2: // Printout tasks
                printTaskSelection();
                break;
            case 3: // Update tasks
                updateTaskSelection();
                break;
            case 4: // Delete tasks
                deleteTaskSelection();
                break;
            case 5: // Settings
                settingsSelection();
                break;
            case 6:
                System.exit(0);
        }
    }

    private void addNewTaskSelection() throws FileNotFoundException, ParseException {
        printCaption.getPrintCaption("menu.addTaskMenu");
        int selection = inputScanner.getIntegerInput();
        switch (selection){
            case 1:
                taskListOperations.addTask(taskTypeReminder);
                break;
            case 2:
                taskListOperations.addTask(taskTypeMeeting);
                break;
            case 3:
                taskListOperations.addTask(taskTypeReserveTime);
                break;
        }
    }

    private void printTaskSelection(){
        if (!ToDo.allTasks.isEmpty()) {
            printCaption.getPrintCaption("menu.printOut");
            printSelections.printOutSelections(inputScanner.getIntegerInput());
            System.out.println();
        }else {
            printCaption.getPrintCaption("errorMessage.NoTask");
        }
    }

    private void updateTaskSelection() throws FileNotFoundException {
        if (!ToDo.allTasks.isEmpty()) {
            printTasks.printOutTasks();
            printCaption.getPrintCaption("message.updateTask");
            int updateTaskSelection = inputScanner.getIntegerInput();
            taskListOperations.updateTask(updateTaskSelection);
        } else {
            printCaption.getPrintCaption("errorMessage.NoTask");
        }
    }

    private void deleteTaskSelection() throws FileNotFoundException {
        if (!ToDo.allTasks.isEmpty()) {
            printCaption.getPrintCaption("message.deleteTask");
            printTasks.printOutTasks();
            int id = inputScanner.getIntegerInput();
            taskListOperations.deleteTask(id);
        } else {
            printCaption.getPrintCaption("errorMessage.NoTask");
        }
    }

    private void settingsSelection(){
        printCaption.getPrintCaption("message.reminderInterval");
        taskReminder.reminderTimeChange(inputScanner.getIntegerInput());
    }
}
