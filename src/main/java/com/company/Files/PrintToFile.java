package com.company.Files;

import java.io.FileNotFoundException;

public interface PrintToFile {
    void putInformationToFile(int number) throws FileNotFoundException;
    void printToFileAfterDeletedTask(int position) throws FileNotFoundException;
    void updateInformation(int number);
}
