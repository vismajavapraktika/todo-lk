package com.company.Files;

import com.company.Captions.CaptionService;
import com.company.Captions.DefaultCaptionService;
import com.company.ToDo;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintWriter;

@Service
public class PrintToTextFile implements PrintToFile{
    private PrintWriter printWriter;
    private CaptionService captionService = new DefaultCaptionService();
    private File file = new File(captionService.getCaption("file.name"));

    @Override
    public void putInformationToFile(int number) throws FileNotFoundException {
        printWriter = new PrintWriter(new FileOutputStream(file,true));
        printWriter.append(ToDo.allTasks.get(number).toString());
        printWriter.println();
        printWriter.close();
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    @Override
    public void printToFileAfterDeletedTask(int position) throws FileNotFoundException {
        printWriter = new PrintWriter(new FileOutputStream(file),false);
        for (int i = 0; i < ToDo.allTasks.size(); i++){
            printWriter.append(ToDo.allTasks.get(i).toString());
            printWriter.println();
        }
        printWriter.close();
    }

    @Override
    public void updateInformation(int number) {
        try {
            printToFileAfterDeletedTask(number);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}