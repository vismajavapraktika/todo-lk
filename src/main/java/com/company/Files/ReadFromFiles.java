package com.company.Files;

import java.io.IOException;

public interface ReadFromFiles {
    void getInformation();
    boolean FileExists(String fileName);
}
