package com.company.Files;

import com.company.Tasks.Meeting;
import com.company.Tasks.Reminder;
import com.company.Tasks.ReserveTime;
import com.company.ToDo;

import java.io.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;

public class ReadFromTextFile implements ReadFromFiles {
    private String fileName = "ToDoList.txt";

    @Override
    public boolean FileExists(String fileName) {
        File file = new File(fileName);
        return file.exists() && !file.isDirectory();
    }

    @Override
    public void getInformation() {
        File file = new File(fileName);
        if (file.exists() && !file.isDirectory())
            try {
                try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                    String line;
                    while ((line = reader.readLine()) != null) {
                        String[] splitLine = line.split(";");
                        switch (splitLine[splitLine.length - 1]) {
                            case "reminder":
                                ToDo.allTasks.add(new Reminder(splitLine[1], LocalDateTime.parse(splitLine[2]), Duration.parse(splitLine[3]), splitLine[4],splitLine[5]));
                                break;
                            case "meeting":
                                ArrayList<String> listOfAttendees = new ArrayList<>();
                                String[] tmp = splitLine[4].split(",");
                                Collections.addAll(listOfAttendees, tmp);
                                ToDo.allTasks.add(new Meeting(splitLine[1], LocalDateTime.parse(splitLine[2]), Duration.parse(splitLine[3]), splitLine[4], listOfAttendees, splitLine[6]));
                                break;
                            case "reserve time":
                                ToDo.allTasks.add(new ReserveTime(splitLine[1], LocalDateTime.parse(splitLine[2]), Duration.parse(splitLine[3]), splitLine[4],splitLine[5]));
                                break;
                        }
                    }
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        else {
            PrintWriter printWriter = null;
            try {
                printWriter = new PrintWriter(file);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            assert printWriter != null;
            printWriter.close();
        }
    }
}
