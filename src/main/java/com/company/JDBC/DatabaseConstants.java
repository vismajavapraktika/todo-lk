package com.company.JDBC;

class DatabaseConstants {

    static final String DB_DRIVER = "org.postgresql.Driver";
    static final String DB_URL = "jdbc:postgresql://localhost:5432/ToDo database";
    static final String DB_USERNAME = "Lukas";
    static final String DB_PASSWORD = "123456";
}
