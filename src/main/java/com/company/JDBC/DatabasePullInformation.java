package com.company.JDBC;

import com.company.Files.ReadFromFiles;
import com.company.Tasks.Meeting;
import com.company.Tasks.Reminder;
import com.company.Tasks.ReserveTime;

import java.sql.*;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static com.company.JDBC.DatabaseConstants.*;
import static com.company.JDBC.DatabasePushInformation.listOfAttendeesId;
import static com.company.Tasks.Task.taskTypeMeeting;
import static com.company.Tasks.Task.taskTypeReminder;
import static com.company.Tasks.Task.taskTypeReserveTime;
import static com.company.ToDo.allTasks;

public class DatabasePullInformation implements ReadFromFiles {

    private static final String SELECT_TASKS = "SELECT * FROM tasks;";
    private static final String SELECT_LIST_OF_ATTENDEES = "SELECT * FROM tableofattendees WHERE taskid = ?";

    public DatabasePullInformation() {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getInformation() {
        PreparedStatement statement = null, statement1 = null;
        String name, location, type;
        LocalDateTime startTime;
        Duration duration;
        try (Connection connection = getConnection()){
            statement = connection.prepareStatement(SELECT_TASKS);
            ResultSet resultSet = statement.executeQuery();
            /*if(!resultSet.next()){
                return;
            }*/
            while (resultSet.next()){
                int id = resultSet.getInt("id");
                name = resultSet.getString("name");
                String tmpStartTime = resultSet.getString("startTime");
                if (tmpStartTime.equals("null")){
                    startTime = null;
                }else {
                    startTime = LocalDateTime.parse(tmpStartTime);
                }
                String tmpDuration = resultSet.getString("duration");
                if (tmpDuration.equals("null")){
                    duration = null;
                }else {
                    duration = Duration.parse(tmpDuration);
                }
                location = resultSet.getString("location");
                type = resultSet.getString("type");
                if (type.equals(taskTypeMeeting)){
                    statement1 = connection.prepareStatement(SELECT_LIST_OF_ATTENDEES);
                    statement1.setInt(1,id);
                    ResultSet resultSet1 = statement1.executeQuery();
                    /*if(!resultSet1.next()){
                        return;
                    }*/
                    resultSet1.next();
                    Array array = resultSet1.getArray("listofattendees");
                    String[] stringArray = (String[]) array.getArray();
                    ArrayList<String> listOfAttendees = new ArrayList<>();
                    if (stringArray!=null){
                        for (int i = 0; i < stringArray.length; i++){
                            listOfAttendees.add(stringArray[i]);
                        }
                    }
                    listOfAttendeesId = resultSet1.getInt("id") + 1;
                    allTasks.add(new Meeting(name, startTime, duration, location, listOfAttendees ,type));
                    resultSet1.close();
                    closeStatement(statement1);

                }else if (type.equals(taskTypeReminder)){
                    allTasks.add(new Reminder(name, startTime, duration, location, type));
                } else if (type.equals(taskTypeReserveTime)){
                    allTasks.add(new ReserveTime(name, startTime, duration, location, type));
                }
                int position = allTasks.size()-1;
                allTasks.get(position).setId(id);
            }
            resultSet.close();
            closeStatement(statement);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean FileExists(String fileName) {
        return true;
    }

    private Connection getConnection(){
        try {
            return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void closeStatement (Statement statement){
        try {
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
