package com.company.JDBC;

import com.company.Files.PrintToFile;
import com.company.ToDo;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.sql.*;

import static com.company.JDBC.DatabaseConstants.*;
import static com.company.Tasks.Task.taskTypeMeeting;

@Service
public class DatabasePushInformation implements PrintToFile {

    private static final String INSERT_TASK = "INSERT INTO tasks(" +
            "name, starttime, duration,location,type)" +
            "VALUES (?,?,?,?,?);";
    private static final String INSERT_LIST_OF_ATTENDEES = "INSERT INTO tableofattendees(" +
            "taskid, listofattendees)" +
            "    VALUES (?, ?);";
    private static final String UPDATE_TASK = "UPDATE tasks SET name = ?, starttime = ?,duration = ?, location = ?,type = ?" +
            " WHERE id = ?";
    private static final String UPDATE_LIST_OF_ATTENDEES = "UPDATE tableofattendees SET taskid = ?, listofattendees = ?" +
            " WHERE taskid = ?";

    private static final String DELETE_ALL_FROM_TABLE_TASK = "DELETE FROM tasks WHERE id = ?;";
    private static final String DELETE_ALL_FROM_TABLE_LIST_OF_ATTENDEES = "DELETE FROM tableofattendees WHERE taskid = ?;";
    static int listOfAttendeesId = 1;

    public DatabasePushInformation() {
        try {
            Class.forName(DB_DRIVER);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }


    @Override
    public void putInformationToFile(int number) throws FileNotFoundException {
        PreparedStatement statement = null;
        try (Connection connection = getConnection()){
            statement = connection.prepareStatement(INSERT_TASK, statement.RETURN_GENERATED_KEYS);
            statement.setString(1, ToDo.allTasks.get(number).getTaskName());
            statement.setString(2, String.valueOf(ToDo.allTasks.get(number).getStartTime()));
            statement.setString(3, String.valueOf(ToDo.allTasks.get(number).getDuration()));
            statement.setString(4, ToDo.allTasks.get(number).getLocation());
            statement.setString(5, ToDo.allTasks.get(number).getType());
            statement.execute();
            if(statement.getGeneratedKeys().next()){
                int taskId = statement.getGeneratedKeys().getInt("id");
                ToDo.allTasks.get(number).setId(taskId);
                if (ToDo.allTasks.get(number).getType().equals(taskTypeMeeting)){
                    PreparedStatement statement1 = null;
                    statement1 = connection.prepareStatement(INSERT_LIST_OF_ATTENDEES);
                    listOfAttendeesId++;
                    statement1.setInt(1, taskId);
                    String[] stringTmpArray = new String[ToDo.allTasks.get(number).getListOfAttendees().size()];
                    for (int i = 0; i < ToDo.allTasks.get(number).getListOfAttendees().size();i++){
                        stringTmpArray[i] = ToDo.allTasks.get(number).getListOfAttendees().get(i);
                    }
                    Array tmp = connection.createArrayOf("text",stringTmpArray);
                    statement1.setArray(2,tmp);
                    statement1.execute();
                    closeStatement(statement1);
                }
            }
            closeStatement(statement);


        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    public void printToFileAfterDeletedTask(int position) throws FileNotFoundException {
        PreparedStatement statement = null;
        try (Connection connection = getConnection()){
            statement = connection.prepareStatement(DELETE_ALL_FROM_TABLE_LIST_OF_ATTENDEES);
            statement.setInt(1,position);
            statement.execute();
            statement = connection.prepareStatement(DELETE_ALL_FROM_TABLE_TASK);
            statement.setInt(1,position);
            statement.execute();
            closeStatement(statement);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        /*for (int i = 0; i < ToDo.allTasks.size(); i++){
            putInformationToFile(i);
        }*/
    }

    @Override
    public void updateInformation(int number) {
        PreparedStatement statement = null;
        try (Connection connection = getConnection()){
            statement = connection.prepareStatement(UPDATE_TASK);
            statement.setString(1,ToDo.allTasks.get(number).getTaskName());
            statement.setString(2, String.valueOf(ToDo.allTasks.get(number).getStartTime()));
            statement.setString(3, String.valueOf(ToDo.allTasks.get(number).getDuration()));
            statement.setString(4,ToDo.allTasks.get(number).getLocation());
            statement.setString(5,ToDo.allTasks.get(number).getType());
            statement.setInt(6, ToDo.allTasks.get(number).getId());
            statement.executeUpdate();
            closeStatement(statement);
            if (ToDo.allTasks.get(number).getType().equals(taskTypeMeeting)){
                statement = connection.prepareStatement(UPDATE_LIST_OF_ATTENDEES);
                for (int i = 1; i < ToDo.allTasks.get(number).getListOfAttendees().size(); i++) {
                    statement.setInt(1, ToDo.allTasks.get(number).getId()); // TODO need to change <--------------
                    String[] stringTmpArray = new String[ToDo.allTasks.get(number).getListOfAttendees().size()];
                    for (int j = 0; j < ToDo.allTasks.get(number).getListOfAttendees().size();j++){
                        stringTmpArray[j] = ToDo.allTasks.get(number).getListOfAttendees().get(j);
                    }
                    Array tmp = connection.createArrayOf("text",stringTmpArray);
                    statement.setArray(2,tmp);
                    statement.setInt(3,ToDo.allTasks.get(number).getId());
                    statement.executeUpdate();
                    closeStatement(statement);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private Connection getConnection(){
        try {
            return DriverManager.getConnection(DB_URL, DB_USERNAME, DB_PASSWORD);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    private void closeStatement (Statement statement){
        try {
            statement.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
