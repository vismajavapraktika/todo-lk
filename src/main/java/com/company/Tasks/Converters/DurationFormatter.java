package com.company.Tasks.Converters;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.time.Duration;

@Component
public class DurationFormatter implements Converter<Duration, String> {
    @Override
    public String convert(Duration duration) {
        return String.valueOf(duration.toMinutes());
    }
}
