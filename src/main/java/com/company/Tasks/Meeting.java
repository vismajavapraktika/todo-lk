package com.company.Tasks;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class Meeting extends Task {
    private ArrayList<String> listOfAttendees;
//    Class constructors

    public Meeting(String taskName, LocalDateTime startTime, Duration duration, String location, ArrayList<String> listOfAttendees, String type) {
        super(taskName, startTime, duration, location, type);
        this.listOfAttendees = listOfAttendees;
    }


    @Override
    public void setListOfAttendees(ArrayList<String> listOfAttendees) {
        this.listOfAttendees = listOfAttendees;
    }


    @Override
    public ArrayList<String> getListOfAttendees() {
        return listOfAttendees;
    }

    @Override
    public String toString() {
        return getTaskName() + ";" + getStartTime() + ";" + getDuration() + ";" + getLocation() + ";" + getListOfAttendees().toString() + ";" + getType();
    }

    @Override
    public String toStringPrintOut() {
        return getId() + ".     " + getTaskName() + "   starts at " + getStartTime() + " duration " + getDuration() + " at location " + getLocation() + " with " + getListOfAttendees() + " " + getType().toUpperCase();
    }

    public String toStringByName(){
        return getTaskName() + "   starts at " +getStartTime().format(DateTimeFormatter.ofPattern("yyyy:MM:dd:HH:mm")) + " duration " + getDuration().toMinutes() + " minutes at location " + getLocation() + " list of attendees " + getListOfAttendees().toString() + " " + getType().toUpperCase();
    }
}
