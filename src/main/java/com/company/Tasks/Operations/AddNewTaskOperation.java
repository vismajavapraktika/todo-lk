package com.company.Tasks.Operations;

import com.company.Captions.PrintCaption;
import com.company.Data.InputScanner;
import com.company.Tasks.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

@Component
public class AddNewTaskOperation {

    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private PrintCaption printCaption;
    @Autowired
    private Task task;
    public String getAddNewTaskName(String taskType){
        printCaption.getPrintCaption("taskElement.name");
        String name = inputScanner.getStringInput();
        if (task.nameCheck(name,taskType)){
            return name;
        }
        return null;
    }

    public LocalDateTime getAddNewTaskStartTime(String taskType){
        boolean correctStartTime = true;
        while (correctStartTime) {
            printCaption.getPrintCaption("taskElement.startTime");
            String startTime = inputScanner.getStringInput();
            if (task.startTimeCheck(startTime,taskType)) {
                if (startTime.isEmpty())
                    return null;
                return LocalDateTime.parse(startTime,DateTimeFormatter.ofPattern("yyyy:MM:dd:HH:mm"));
            }
        }
        return null;
    }

    public Duration getAddNewTaskDuration(String taskType) {
        boolean correctDuration = true;
        while (correctDuration) {
            printCaption.getPrintCaption("taskElement.duration");
            String duration = inputScanner.getStringInput();
            if (task.durationCheck(duration,taskType)) {
                if (duration.isEmpty()){
                    return null;
                }
                return Duration.ofMinutes(Long.parseLong(duration));
            }
        }
        return null;
    }

    public String getAddNewTaskLocation(){
        printCaption.getPrintCaption("taskElement.location");
        String location = inputScanner.getStringInput();
        if (location.isEmpty()) location = "NO LOCATION";
        return location;
    }

    public ArrayList<String> getAddNewTaskListOfAttendees(){
        ArrayList<String> listOfAttendees = new ArrayList<>();
        printCaption.getPrintCaption("taskElement.numberOfAttendees");
        int numberOfAttendees = inputScanner.getIntegerInput();
        for (int i = 0; i < numberOfAttendees; i++) {
            System.out.println("Enter " + (i + 1) + " attendee");
            listOfAttendees.add(inputScanner.getStringInput());
        }
        return listOfAttendees;
    }
}
