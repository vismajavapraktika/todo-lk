package com.company.Tasks.Operations;

import com.company.Captions.PrintCaption;
import com.company.Data.DataChecker;
import com.company.Data.InputScanner;
import com.company.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

import static com.company.Tasks.Task.taskTypeReminder;

@Component
public class UpdateTaskOperation {
    public static int TaskThatIsUpdatingId;
    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private DataChecker dataChecker;
    @Autowired
    private PrintCaption printCaption;

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy:MM:dd:HH:mm");

    public String getUpdateTaskName(int id){
        printCaption.getPrintCaption("taskElement.name");
        String name = inputScanner.getStringInput();
        if (!name.isEmpty()) {
            return name;
        }
        else{
            System.out.print(ToDo.allTasks.get(id).getTaskName());
            printCaption.getPrintCaption("message.oldNameLeft");
            return ToDo.allTasks.get(id).getTaskName();
        }
    }

    public LocalDateTime getUpdateTaskStartTime(int id) {
        boolean correctTaskStartTime = true;
        TaskThatIsUpdatingId = id + 1;
        while (correctTaskStartTime) {
            printCaption.getPrintCaption("taskElement.startTime");
            String startTime = inputScanner.getStringInput();
            if (dataChecker.inputChecker("Date", startTime, ToDo.allTasks.get(id).getType())){
                if (!startTime.isEmpty()) {
                    return LocalDateTime.parse(startTime,formatter);
                }
            }
            if (startTime.isEmpty() && ToDo.allTasks.get(id).getType().equals(taskTypeReminder)){
                System.out.println("Do you want to use old start time? (" + ToDo.allTasks.get(id).getStartTime() +") (Yes/No)");
                if (inputScanner.getStringInput().toLowerCase().equals("no")) {
                    return null;
                } else {
                    return ToDo.allTasks.get(id).getStartTime();
                }
            } else if (startTime.isEmpty()){
                return ToDo.allTasks.get(id).getStartTime();
            }
        }
        return null;
    }

    public Duration getUpdateTaskDuration(int id) {
        while (true) {
            printCaption.getPrintCaption("taskElement.duration");
            String duration = inputScanner.getStringInput();
            if (dataChecker.inputChecker("DurationTime",duration,ToDo.allTasks.get(id).getType())) {
                if (!duration.isEmpty()) {
                    return Duration.ofMinutes(Long.parseLong(duration));
                }
            }
            if (duration.isEmpty() && ToDo.allTasks.get(id).getType().equals(taskTypeReminder)){
                System.out.println("Do you want to use old duration? (" + ToDo.allTasks.get(id).getDuration() +") (Yes/No)");
                if (inputScanner.getStringInput().toLowerCase().equals("no")){
                    return null;
                } else {
                    return ToDo.allTasks.get(id).getDuration();
                }
            } else if (duration.isEmpty()){
                return ToDo.allTasks.get(id).getDuration();
            }
        }
    }

    public String getUpdateTaskLocation(int id){
        printCaption.getPrintCaption("taskElement.location");
        String location = inputScanner.getStringInput();
        if (location.isEmpty()) {
            System.out.println("Do you want to use old location? (" + ToDo.allTasks.get(id).getLocation() +") (Yes/No)");
            if (inputScanner.getStringInput().toLowerCase().equals("no")){
                return "";
            } else {
                return ToDo.allTasks.get(id).getLocation();
            }
        }
        return location;
    }

    public ArrayList<String> getUpdateTaskListOfAttendees(int id){
        ArrayList<String> listOfAttendees;
        printCaption.getPrintCaption("taskElement.numberOfAttendeesChange");
        if (inputScanner.getStringInput().toLowerCase().equals("no")){
            listOfAttendees = ToDo.allTasks.get(id).getListOfAttendees();
        } else {
            printCaption.getPrintCaption("taskElement.numberOfAttendees");
            listOfAttendees = new ArrayList<>();
            int number = inputScanner.getIntegerInput();
            if (number > 0) {
                for (int i = 0; i < number; i++) {
                    System.out.print(i+1);
                    printCaption.getPrintCaption("taskElement.listOfAttendees");
                    listOfAttendees.add(inputScanner.getStringInput());
                }
            }
        }
        return listOfAttendees;
    }
}
