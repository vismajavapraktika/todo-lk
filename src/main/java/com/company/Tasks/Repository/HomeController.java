package com.company.Tasks.Repository;

import com.company.Tasks.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private TaskService taskService;

    @ModelAttribute("allTasks")
    public List<Task> allTasks() {
        return taskService.findAllTasks();
    }

    @RequestMapping("/")
    public String home() {
        return "index";
    }
}
