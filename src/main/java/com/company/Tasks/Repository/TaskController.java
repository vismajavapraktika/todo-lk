package com.company.Tasks.Repository;

import com.company.Tasks.Task;
import com.company.Tasks.TaskListOperations;
import com.company.Tasks.TaskListOperationsConsole;
import com.company.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

@Controller
public class TaskController {

    @Autowired
    private TaskService taskService;
    @Autowired
    private TaskListOperationsConsole taskListOperationsConsole;

    @RequestMapping("/task/{id}")
    public String viewTask(@PathVariable int id, Model model){
        int position = taskListOperationsConsole.getListElementsNumber(id);
        model.addAttribute("task",taskService.getTask(position));
        return "Tasks/view";
    }

    @RequestMapping(value = "/task/new", method = RequestMethod.GET)
    public String newTaskPage(Task task){
        return "Tasks/edit";
    }

    @RequestMapping(value = "/task/new", method = RequestMethod.POST)
    public String saveNewTask(Task task, BindingResult result){
        task.setStartTime(LocalDateTime.parse(task.getTmpStartTime()));
        task.setDuration(Duration.ofMinutes(Long.parseLong(task.getTmpDuration())));
        if (task.getId() > 0){
            taskService.updateTask(task);
        }
        taskService.saveNewTask(task);
        return "redirect:/";
    }

    @RequestMapping(value = "/task/delete/{id}", method = RequestMethod.GET)
    public String deleteTask(Task task){
        taskService.delete(task);
        return "redirect:/";
    }

    @RequestMapping(value = "/task/update/{id}",method = RequestMethod.GET)
    public String updateTask(Task task, Model model){
        int position = taskListOperationsConsole.getListElementsNumber(task.getId());
        taskService.getTask(position).setTmpStartTime(String.valueOf(taskService.getTask(position).getStartTime()));
        taskService.getTask(position).setTmpDuration(String.valueOf(taskService.getTask(position).getDuration().toMinutes()));
        taskService.getTask(position).setTmpListOfAttendees(String.valueOf(taskService.getTask(position).getListOfAttendees()));
        taskService.getTask(position).setId(taskService.getTask(position).getId());
        Task task1 = taskService.getTask(position);
        model.addAttribute("task",task1);
        return "Tasks/edit";
    }
}
