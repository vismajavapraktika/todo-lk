package com.company.Tasks.Repository;

import com.company.Captions.CaptionService;
import com.company.Tasks.Task;
import com.company.Tasks.TaskListOperationsWeb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.util.List;

import static com.company.ToDo.allTasks;
import static com.company.ToDo.getAllTasks;

@Service
public class TaskService {
    @Autowired
    private CaptionService captionService;
    @Autowired
    private TaskListOperationsWeb taskListOperationsWeb;

    public List<Task> findAllTasks() {
        return getAllTasks();
    }

    public Task getTask(int id){
        return allTasks.get(id);
    }

    public Task saveNewTask(Task task) {
        return taskListOperationsWeb.addTask(task);
    }

    public void delete(Task task) {
        try {
            taskListOperationsWeb.deleteTask(task.getId());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public void updateTask(Task task){
        try {
            taskListOperationsWeb.updateTask(task.getId());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }
}
