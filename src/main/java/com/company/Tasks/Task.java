package com.company.Tasks;


import com.company.Data.DataChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cglib.core.Local;
import org.springframework.stereotype.Component;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

import static com.company.ToDo.allTasks;

@Component
public class Task {

    public static final String taskTypeMeeting = "meeting", taskTypeReminder = "reminder", taskTypeReserveTime = "reserve time";
    private int MINUTE_IN_MILISECONDS = 60000;

    private String taskName;

//    private String startTime;
    private LocalDateTime startTime;
//    private String duration;
    private Duration duration;
    private String tmpStartTime;
    private String tmpDuration;
    private String tmpListOfAttendees;
    private String location;
    private String type;
    private boolean checked;
    private int id;
    private UUID uuid;

    @Autowired
    private DataChecker dataChecker;


    @SuppressWarnings("WeakerAccess")
    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

//    public void setStartTime(String startTime) {
//        this.startTime = startTime;
//    }

    public void setStartTime(LocalDateTime startTime){
        this.startTime = startTime;
    }

//    public void setDuration(String duration) {
//        this.duration = duration;
//    }

    public void setDuration(Duration duration){
        this.duration = duration;
    }

    @SuppressWarnings("WeakerAccess")
    public void setLocation(String location) {
        this.location = location;
    }

    private void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void setListOfAttendees(ArrayList<String> listOfAttendees){}

    public void setType(String type) {
        this.type = type;
    }

    public void setTmpStartTime(String tmpStartTime) {
        this.tmpStartTime = tmpStartTime;
    }

    public void setTmpDuration(String tmpDuration) {
        this.tmpDuration = tmpDuration;
    }

    public void setTmpListOfAttendees(String tmpListOfAttendees) {
        this.tmpListOfAttendees = tmpListOfAttendees;
    }

    //    Class constructor


    public Task(){}

    public Task(String taskName, /*String startTime*/ LocalDateTime startTime, /*String*/ Duration duration, String location, String type) {
        this.taskName = taskName;
        this.startTime = startTime;
        this.duration = duration;
        this.location = location;
        this.type = type;
        this.checked = false;
    }

    public void setId(int id) {
        this.id = id;
    }

    //    -----------------------------------------
//    Getters
    public String getTaskName() {
        return taskName;
    }

//    public String getStartTime() {
//        return startTime;
//    }

    public LocalDateTime getStartTime(){return startTime;}

//    public String getDuration() {
//        return duration;
//    }

    public Duration getDuration(){
        return duration;
    }

    public String getLocation() {
        return location;
    }

    public String getType() {
        return type;
    }

    public int getId() {
        return id;
    }

    public String getTmpStartTime() {
        return tmpStartTime;
    }

    public String getTmpDuration() {
        return tmpDuration;
    }

    public String getTmpListOfAttendees() {
        return tmpListOfAttendees;
    }

    public ArrayList<String> getListOfAttendees(){return null;}

    /*public Date getEndTime() throws ParseException {
        //DateFormat format = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        //Date startTime = format.parse(this.getStartTime());
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy:MM:dd:HH:mm");
        LocalDateTime startTime = formatter.parse(this.getStartTime());
        String[] splitDurationTime =  this.getDuration().split(":");
        int durationTime = Integer.parseInt(splitDurationTime[0]) * 60 + Integer.parseInt(splitDurationTime[1]);
        return new Date(startTime.getTime() + (durationTime * 60000));
    }*/
    public LocalDateTime getEndTime() throws ParseException {
        return (this.getStartTime().plus(this.getDuration()));
    }

    public boolean isChecked() {
        return checked;
    }
// toString method

    public String toStringPrintOut(){
        if (getStartTime() == null){
            return this.getId() + ".     " + getTaskName() + "   starts at " + "" + " duration " + getDuration().toMinutes() + " at location " + getLocation() + " " + getType().toUpperCase();
        }
        return this.getId() + ".     " + getTaskName() + "   starts at " + getStartTime().format(DateTimeFormatter.ofPattern("yyyy:MM:dd:HH:mm")) + " duration " + getDuration().toMinutes()
                + " minutes at location " + getLocation() + " " + getType().toUpperCase();
    }

    public String toStringByName(){
        if (getStartTime() == null){
            if (getDuration() == null){
                return this.getId() + ".     " + getTaskName() + "   starts at " + "" + " duration " + ""
                        + " at location " + getLocation() + " " + getType().toUpperCase();
            }
            return this.getId() + ".     " + getTaskName() + "   starts at " + "" + " duration " + getDuration().toMinutes() + " at location " + getLocation() + " " + getType().toUpperCase();
        }
        if (getDuration() == null){
            return this.getId() + ".     " + getTaskName() + "   starts at " + getStartTime().format(DateTimeFormatter.ofPattern("yyyy:MM:dd:HH:mm")) + " duration " + ""
                    + " at location " + getLocation() + " " + getType().toUpperCase();
        }
        return getTaskName() + "   starts at " + getStartTime().format(DateTimeFormatter.ofPattern("yyyy:MM:dd:HH:mm")) + " duration " + getDuration().toMinutes()
                + " minutes at location " + getLocation() + " " + getType().toUpperCase();
    }

    String toStringTimeUntilStart(){
        return getType() + " named " + getTaskName() + " will start in ";
    }

    @SuppressWarnings("WeakerAccess")
    public void changeChecker(){
        if (!isChecked()){
            this.setChecked(true);
        }
    }

    public boolean nameCheck(String taskName, String taskType){
        return dataChecker.inputChecker("Name",taskName,taskType);
    }

    public boolean startTimeCheck(String taskStartTime, String taskType){
        return dataChecker.inputChecker("Date", taskStartTime,taskType);
    }

    public boolean durationCheck(String taskDuration, String taskType){
        return dataChecker.inputChecker("DurationTime", taskDuration,taskType);
    }

    public boolean overlappingCheck(LocalDateTime taskStartTime, Duration taskDuration, String taskType){
        try {
            return dataChecker.overlappingTimeCheck(taskStartTime, taskDuration, taskType);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void addToListTasks(String taskName, LocalDateTime taskStartTime, Duration taskDuration, String taskLocation, String taskType){
        switch (taskType){
            case "reminder":
                allTasks.add(new Reminder(taskName,taskStartTime,taskDuration,taskLocation,taskType));
                break;
            case "reserve time":
                allTasks.add(new ReserveTime(taskName,taskStartTime,taskDuration,taskLocation,taskType));
                break;
        }
    }

    public void addToListTasks(String taskName, LocalDateTime taskStartTime, Duration taskDuration, String taskLocation, ArrayList<String> taskListOfAttendees, String taskType){
        allTasks.add(new Meeting(taskName,taskStartTime,taskDuration,taskLocation,taskListOfAttendees,taskType));
    }
}