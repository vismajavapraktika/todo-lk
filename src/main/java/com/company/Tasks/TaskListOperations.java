package com.company.Tasks;

import java.io.FileNotFoundException;
import java.text.ParseException;

public interface TaskListOperations {
    void addTask(String taskType) throws ParseException, FileNotFoundException;
    void deleteTask(final int id) throws FileNotFoundException;
    void updateTask(final int id) throws FileNotFoundException;
}
