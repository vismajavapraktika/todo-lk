package com.company.Tasks;

import com.company.Captions.PrintCaption;
import com.company.Files.PrintToFile;
import com.company.Files.ReadFromFiles;
import com.company.JDBC.DatabasePullInformation;
import com.company.Tasks.Operations.AddNewTaskOperation;
import com.company.Tasks.Operations.UpdateTaskOperation;
import com.company.ToDo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;

import static com.company.Tasks.Task.taskTypeMeeting;
import static com.company.Tasks.Task.taskTypeReminder;
import static com.company.Tasks.Task.taskTypeReserveTime;
import static com.company.ToDo.*;

@Service
public class TaskListOperationsConsole implements TaskListOperations {
    @Autowired
    private ReadFromFiles readFromFiles;
    @Autowired
    private PrintToFile printToFile;
    @Autowired
    private Task taskClass;
    @Autowired
    AddNewTaskOperation addNewTaskOperation;
    @Autowired
    private PrintCaption printCaption;
    @Autowired
    private UpdateTaskOperation updateTaskOperation;

    @Override
    public void addTask(String taskType) throws ParseException, FileNotFoundException {
        working = false;
        if (readFromFiles.FileExists(fileName)) {
            String taskName = addNewTaskOperation.getAddNewTaskName(taskType);
            LocalDateTime taskStartTime;
            Duration taskDuration;
            while (true) {
                taskStartTime = addNewTaskOperation.getAddNewTaskStartTime(taskType);
                taskDuration = addNewTaskOperation.getAddNewTaskDuration(taskType);
                if (taskClass.overlappingCheck(taskStartTime, taskDuration, taskType)) break;
                else {
                    System.out.println("Time is overlapping");
                }
            }
            String taskLocation = addNewTaskOperation.getAddNewTaskLocation();
            switch (taskType) {
                case taskTypeMeeting: {
                    ArrayList<String> taskListOfAttendees = addNewTaskOperation.getAddNewTaskListOfAttendees();
                    taskClass.addToListTasks(taskName, taskStartTime, taskDuration, taskLocation, taskListOfAttendees, taskType);
                    printToFile.putInformationToFile(allTasks.size() - 1);
                    break;
                }
                case taskTypeReminder: {
                    taskClass.addToListTasks(taskName, taskStartTime, taskDuration, taskLocation, taskType);
                    printToFile.putInformationToFile(allTasks.size() - 1);
                    break;
                }
                case taskTypeReserveTime: {
                    taskClass.addToListTasks(taskName, taskStartTime, taskDuration, taskLocation, taskType);
                    printToFile.putInformationToFile(allTasks.size() - 1);
                    break;
                }
            }
        }
        working = true;

    }

    @Override
    public void deleteTask(int id) throws FileNotFoundException {
        int position = getListElementsNumber(id);
        if (position >= 0) {
            allTasks.remove(position);
            printToFile.printToFileAfterDeletedTask(id);
        } else {
            printCaption.getPrintCaption("errorMessage.NothingToDelete");
        }
    }

    public int getListElementsNumber(int id) {
        for (int i = 0; i < allTasks.size(); i++){
            if (id == allTasks.get(i).getId()){
                return i;
            }
        }
        return -1;
    }

    private void idDecrease(int id){
        int newId = id;
        if (allTasks.size() > id+1){
            for (int i = id + 1; i < allTasks.size(); i++){
                allTasks.get(i).setId(newId+1);
                newId++;
            }
        }
        else {
            //allTasks.get(id).setIdCounter(id+1);
        }
    }

    @SuppressWarnings("LoopStatementThatDoesntLoop")
    @Override
    public void updateTask(int id) throws FileNotFoundException{
        String name, location;
        LocalDateTime startTime;
        Duration duration;
        ArrayList<String> listOfAttendees;
        boolean checkIfNotOverlapping = true;
        int position = getListElementsNumber(id);
        name = updateTaskOperation.getUpdateTaskName(position);
        while (checkIfNotOverlapping){
            startTime = updateTaskOperation.getUpdateTaskStartTime(position);
            duration = updateTaskOperation.getUpdateTaskDuration(position);
            ToDo.allTasks.get(position).setStartTime(startTime);
            ToDo.allTasks.get(position).setDuration(duration);
            if (taskClass.overlappingCheck(startTime,duration,ToDo.allTasks.get(position).getType())) break;
            else {
                System.out.println("Time is overlapping");
            }
        }
        location = updateTaskOperation.getUpdateTaskLocation(position);
        if (ToDo.allTasks.get(position).getType().equals(taskTypeMeeting)){
            listOfAttendees = updateTaskOperation.getUpdateTaskListOfAttendees(position);
            ToDo.allTasks.get(position).setListOfAttendees(listOfAttendees);
        }
        ToDo.allTasks.get(position).setTaskName(name);
        ToDo.allTasks.get(position).setLocation(location);
//        int updatePosition = ToDo.allTasks.get(id).getId()-1; // -1 because array starts from 0 and id from 1
        printToFile.updateInformation(position);
    }
}
