package com.company.Tasks;
import com.company.Captions.PrintCaption;
import com.company.Files.PrintToFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import static com.company.ToDo.allTasks;

@Service
public class TaskListOperationsWeb implements TaskListOperations {

    @Autowired
    private PrintToFile printToFile;

    @Autowired
    private PrintCaption printCaption;

    @Override
    public void addTask(String taskType) throws ParseException, FileNotFoundException {

    }

    public Task addTask(Task task){
        if (task != null) {
            switch (task.getType()) {
                case "reminder":
                    allTasks.add(new Reminder(task.getTaskName(), task.getStartTime(), task.getDuration(), task.getLocation(), task.getType()));
                    try {
                        printToFile.putInformationToFile(allTasks.size()-1);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;
                case "meeting":
                    ArrayList<String> tmpListOfAttendees = new ArrayList<>();
                    System.out.println(task.getTmpListOfAttendees());
                    String[] tokens = task.getTmpListOfAttendees().split(";");
                    for (int i = 0; i < tokens.length; i++){
                        tmpListOfAttendees.add(tokens[i]);
                    }
//                    task.setListOfAttendees(tmpListOfAttendees);
                    allTasks.add(new Meeting(task.getTaskName(), task.getStartTime(), task.getDuration(), task.getLocation(),tmpListOfAttendees, task.getType()));
                    try {
                        printToFile.putInformationToFile(allTasks.size() - 1);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;
                case "reserve time":
                    allTasks.add(new ReserveTime(task.getTaskName(), task.getStartTime(), task.getDuration(), task.getLocation(), task.getType()));
                    try {
                        printToFile.putInformationToFile(allTasks.size() - 1);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } else {
            System.out.println("TASK IS NULL");
        }
        return task;
    }

    @Override
    public void deleteTask(int id) throws FileNotFoundException {
        int position = getListElementsNumber(id);
        if (position >= 0){
            allTasks.remove(position);
            printToFile.printToFileAfterDeletedTask(id);
        } else{
            printCaption.getPrintCaption("errorMessage.NothingToDelete");
        }
    }

    public int getListElementsNumber(int id) {
        for (int i = 0; i < allTasks.size(); i++){
            if (id == allTasks.get(i).getId()){
                return i;
            }
        }
        return -1;
    }

    @Override
    public void updateTask(int id) throws FileNotFoundException {

    }
}
