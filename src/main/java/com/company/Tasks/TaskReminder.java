package com.company.Tasks;

import com.company.Data.DataChecker;
import com.company.ToDo;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;

import static com.company.ToDo.working;
import static java.time.LocalDateTime.now;

public class TaskReminder extends Thread {
//    @Autowired
//    private DataChecker dataChecker;
    private volatile static int reminderTime = 60;
    private static final int MILLISECONDS_TO_MINUTES = 60000;

    @SuppressWarnings("InfiniteLoopStatement")
    @Override
    public void run() {
        while (!Thread.currentThread().isInterrupted()) {
            if (working){
                for (int i = 0; i < ToDo.allTasks.size(); i++) {
                    try {
                        taskReminderBeforeAnHour(i);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    private void taskReminderBeforeAnHour(int id) throws ParseException {
        if (ToDo.allTasks.get(id).getStartTime() != null && ToDo.allTasks.get(id).getDuration() != null){
//        if (dataChecker.timeIsEmpty(ToDo.allTasks.get(id).getStartTime()) && dataChecker.timeIsEmpty(ToDo.allTasks.get(id).getDuration())) {
//            Date date = new Date();
            LocalDateTime date = now();
//            SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
            LocalDateTime startTime = ToDo.allTasks.get(id).getStartTime();
            if (leftTimeUntilLocalTime(startTime,date)) {
                if (!ToDo.allTasks.get(id).isChecked()) {
                    ToDo.allTasks.get(id).changeChecker();
                    System.out.println(ToDo.allTasks.get(id).toStringTimeUntilStart() + startTime.getMinute() +" minutes at " + ToDo.allTasks.get(id).getStartTime());
                }
            }
        }
    }

    private boolean leftTimeUntilLocalTime(LocalDateTime startTime, LocalDateTime date){
        return startTime.isBefore(date.plusHours(1)) || startTime.isEqual(date.plusHours(1));
    }


    private void setReminderTime(int reminderTime) {
        TaskReminder.reminderTime = reminderTime;
    }

    public void reminderTimeChange(int newTimeInterval){

        this.setReminderTime(newTimeInterval);
    }
}
