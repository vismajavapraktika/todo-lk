package com.company;

import com.company.Captions.PrintCaption;
import com.company.Data.InputScanner;
import com.company.Data.StartMenuHandler;
import com.company.Files.ReadFromFiles;
import com.company.Tasks.TaskReminder;
import com.company.Tasks.Task;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.io.*;
import java.text.ParseException;
import java.util.*;


@Component
public class ToDo implements CommandLineRunner {
    @Autowired
    private StartMenuHandler startMenuHandler;
    @Autowired
    private ReadFromFiles readFromFiles;
    @Autowired
    private InputScanner inputScanner;
    @Autowired
    private PrintCaption printCaption;

    public static List<Task> allTasks = new ArrayList<>();
    public static String fileName;
    public static boolean working = true;

    void start(String fileName) throws IOException, ParseException {
        readFromFiles.getInformation();
        new TaskReminder().start();
        System.out.println("ToDo List application");
        boolean programIsWorking = true;
        while(programIsWorking) {
            if (!allTasks.isEmpty()) {
                printCaption.getPrintCaption("menu.mainMenu");
                int selection = inputScanner.getIntegerInput();
                startMenuHandler.start(selection);
            } else {
                printCaption.getPrintCaption("message.firstLogin");
                int addTaskCaseSelection = 1;
                startMenuHandler.start(addTaskCaseSelection);
            }
        }
    }


    @Override
    public void run(String... strings) throws Exception {
        start(fileName);
    }

    public static List<Task> getAllTasks() {
        return allTasks;
    }
}
