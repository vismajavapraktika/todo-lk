package com.company.UnitTests;

import com.company.Data.DataChecker;
import org.junit.Test;

import static org.junit.Assert.*;

public class DataCheckerTest {
    @Test
    public void inputChecker() throws Exception {
        DataChecker dataChecker = new DataChecker();
        assertTrue("Date is true", dataChecker.inputChecker("Date","2016:10:10:12:00","reminder"));
        assertTrue("DurationTime is true", dataChecker.inputChecker("DurationTime","12:00","reminder"));
        assertTrue("Date is true", dataChecker.inputChecker("Date","","reminder"));
        assertTrue("DurationTime is true", dataChecker.inputChecker("DurationTime","","reminder"));

        assertTrue("Date is true meeting", dataChecker.inputChecker("Date","2016:10:10:12:00","meeting"));
        assertTrue("Date is true meeting", dataChecker.inputChecker("DurationTime","12:00","meeting"));
        assertFalse("Date is true meeting", dataChecker.inputChecker("Date","","meeting"));
        assertFalse("Date is true meeting", dataChecker.inputChecker("DurationTime","","meeting"));

        assertTrue("Date is true reserve time", dataChecker.inputChecker("Date","2016:10:10:12:00","reserve time"));
        assertTrue("Date is true reserve time", dataChecker.inputChecker("DurationTime","12:00","reserve time"));
        assertFalse("Date is true reserve time", dataChecker.inputChecker("Date","","reserve time"));
        assertFalse("Date is true reserve time", dataChecker.inputChecker("DurationTime","","reserve time"));
    }

}