package com.company.UnitTests;

import com.company.Files.ReadFromFiles;
import com.company.Files.ReadFromTextFile;
import com.company.ToDo;
import org.junit.Test;

import static org.junit.Assert.*;

public class ReadFromTextFileTest {
    private ReadFromFiles readFromFiles = new ReadFromTextFile();
    @Test
    public void fileExists() throws Exception {
        assertTrue("File exist",readFromFiles.FileExists("ToDoList.txt"));
    }

    @Test
    public void readInformationFromFile() throws Exception {
        readFromFiles.getInformation();
        assertTrue("Names should be equal","Atostogos".equals(ToDo.allTasks.get(0).getTaskName()));
        assertTrue("Start time should be equal", "2016:07:26:18:00".equals(ToDo.allTasks.get(0).getStartTime()));
        assertTrue("Duration should be equal", "96:00".equals(ToDo.allTasks.get(0).getDuration()));
        assertTrue("Location should be equal", "NY".equals(ToDo.allTasks.get(0).getLocation()));
    }

}