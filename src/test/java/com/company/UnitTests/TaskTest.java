package com.company.UnitTests;

import com.company.Tasks.Task;
import org.junit.Test;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;

import static org.junit.Assert.*;

public class TaskTest {

    @Test
    public void constructorTest() throws Exception {
        String name = "name", startTime = "2016:10:10:12:10", duration = "20", location = "Vilnius",type = "reminder";
        Task testTask = new Task(name, LocalDateTime.parse(startTime), Duration.parse(duration), location, type);
        assertTrue("Equal names",testTask.getTaskName().equals(name));
        assertTrue("Equal start times",testTask.getStartTime().equals(startTime));
        assertTrue("Equal durations",testTask.getDuration().equals(duration));
        assertTrue("Equal locations",testTask.getLocation().equals(location));
        assertTrue("Equal types",testTask.getType().equals(type));
    }

    @Test
    public void getEndTime() throws Exception {
        Task testTask = new Task();
        testTask.setStartTime(LocalDateTime.parse("2016:12:15:10:20"));
        testTask.setDuration(Duration.parse("1200"));
        SimpleDateFormat format = new SimpleDateFormat("yyyy:MM:dd:HH:mm");
        Date testDate = format.parse("2016:12:16:06:20");
        Date testDate2 = new Date();
        assertTrue("They are equal",testTask.getEndTime().equals(testDate));
        assertFalse("They are not equal", testTask.getEndTime().equals(testDate2));
    }

    @Test
    public void changeChecker() throws Exception {
        Task testTask = new Task();
        testTask.changeChecker();
        assertTrue("Needs to be true", testTask.isChecked());
    }

    @Test
    public void checkerMethodsTest() throws Exception {
        Task taskTest = new Task();
        String name = "Reminders name", startTime = "2016:10:10:12:00", duration = "3000",
                location = "Vilnius", type = "reminder",name2 = "Reserve times name", startTime2 = "2016:10:11:02:00",
                duration2 = "2:00", location2 = "Palanga", type2 = "reserve time",
                name3 = "Meeting name", startTime3 = "2016:10:08:12:00", duration3 = "5880", location3 = "Kaunas",
                type3 = "meeting";
        ArrayList<String> loa = new ArrayList<>();
        loa.add("Jonas");
        loa.add("Petras");
        loa.add("Justas");
        
        assertTrue("Name is a string, can't be bad",taskTest.nameCheck(name,type));
        assertTrue("First time is always true", taskTest.startTimeCheck(startTime,type));
        assertTrue("Duration is true", taskTest.durationCheck(duration,type));
        assertTrue("No overlapping true",taskTest.overlappingCheck(LocalDateTime.parse(startTime),Duration.parse(duration),type));
        taskTest.addToListTasks(name,LocalDateTime.parse(startTime),Duration.parse(duration),location,type);

        assertTrue("Name is a string, can't be bad",taskTest.nameCheck(name2,type2));
        assertTrue("Second time is always true", taskTest.startTimeCheck(startTime2,type2));
        assertTrue("Duration is true", taskTest.durationCheck(duration2,type2));
        assertFalse("No overlapping true",taskTest.overlappingCheck(LocalDateTime.parse(startTime2),Duration.parse(duration2),type2));
        taskTest.addToListTasks(name2,LocalDateTime.parse(startTime2),Duration.parse(duration2),location2,type2);

        assertTrue("Name is a string, can't be bad",taskTest.nameCheck(name3,type3));
        assertTrue("Second time is always true", taskTest.startTimeCheck(startTime3,type3));
        assertTrue("Duration is true", taskTest.durationCheck(duration3,type3));
        assertFalse("No overlapping true",taskTest.overlappingCheck(LocalDateTime.parse(startTime3),Duration.parse(duration3),type3));
        taskTest.addToListTasks(name3,LocalDateTime.parse(startTime3),Duration.parse(duration3),location3,loa,type3);
    }
}